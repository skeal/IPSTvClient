<?
    // Klassendefinition
    class IPSTvClient extends IPSModule {
 
        // Der Konstruktor des Moduls
        // Überschreibt den Standard Kontruktor von IPS
        public function __construct($InstanceID) {
            // Diese Zeile nicht löschen
            parent::__construct($InstanceID);
 
            // Selbsterstellter Code
        }
 
        // Überschreibt die interne IPS_Create($id) Funktion
        public function Create() {
            // Diese Zeile nicht löschen.
            parent::Create();
            
            $this->RegisterPropertyInteger("IPSTvCl_Intervall", 18000);
            $this->RegisterPropertyString("IPSTvCl_Username", "");
            $this->RegisterPropertyString("IPSTvCl_Password", "");
            $this->RegisterPropertyString("IPSTvCl_IP", "");
			$this->RegisterPropertyInteger("IPSTvCl_Port", "");
            $this->RegisterPropertyInteger("IPSTvCl_SrvType", "");
            $this->RegisterTimer("IPSTvCl_ReadTvInfo", 0, 'IPSTvCl_Update($_IPS[\'TARGET\']);');
        }
 
        // Überschreibt die intere IPS_ApplyChanges($id) Funktion
        public function ApplyChanges() {
            // Diese Zeile nicht löschen
            parent::ApplyChanges();
			
			$this->RegisterPropertyInteger("IPSTvCl_Intervall", 18000);
            $this->RegisterPropertyString("IPSTvCl_Username", "");
            $this->RegisterPropertyString("IPSTvCl_Password", "");
            $this->RegisterPropertyString("IPSTvCl_IP", "");
			$this->RegisterPropertyInteger("IPSTvCl_Port", "");
            $this->RegisterPropertyInteger("IPSTvCl_SrvType", "");
            $this->RegisterTimer("IPSTvCl_ReadTvInfo", 0, 'IPSTvCl_Update($_IPS[\'TARGET\']);');
			
			$this->RegisterVariableString("IPSTvCl_Sw", "Softwarestand");
            $this->RegisterVariableInteger("IPSTvCl_Api", "Apiversion");
            $this->RegisterVariableString("IPSTvCl_SrvName", "Servername");
			$this->RegisterVariableInteger("IPSTvCl_RecordsFinished", "Fertige Aufnahmen");
			$this->RegisterVariableInteger("IPSTvCl_RecordsError", "Fehlerhafte Aufnahmen");
			$this->RegisterVariableInteger("IPSTvCl_RecordsSuccess", "Erfolgreiche Aufnahmen");
			$this->RegisterVariableString("IPSTvCl_RecordsHTML", "Aufnahmenliste", "~HTMLBox");
			$this->SetTimerInterval("IPSTvCl_ReadTvInfo", $this->ReadPropertyString("IPSTvCl_Intervall"));
			
        }
 
        /**
        * Die folgenden Funktionen stehen automatisch zur Verfügung, wenn das Modul über die "Module Control" eingefügt wurden.
        * Die Funktionen werden, mit dem selbst eingerichteten Prefix, in PHP und JSON-RPC wiefolgt zur Verfügung gestellt:
        */
        public function Update() {
		    try {
				// Basisinfos abfragen (0 = tvheadend)
			    $getInfos = $this->GetBasicInfo($this->ReadPropertyString("IPSTvCl_SrvType"));
				if (!empty($getInfos)) {
				$this->SetValueString("IPSTvCl_Sw", $getInfos['sw_version']);
				$this->SetValueInteger("IPSTvCl_Api", $getInfos['api_version']);
				$this->SetValueString("IPSTvCl_SrvName", $getInfos['name']);
				}
				// Aufnahmen abfragen (0 = tvheadend)
				$getRecordings = $this->GetRecordings($this->ReadPropertyString("IPSTvCl_SrvType"));
				if (!empty($getRecordings)) {
					$this->SetValueInteger("IPSTvCl_RecordsFinished", $getRecordings["rec_total"]);
					$this->SetValueInteger("IPSTvCl_RecordsError", $getRecordings["rec_error"]);
					$this->SetValueInteger("IPSTvCl_RecordsSuccess", $getRecordings["rec_success"]);
					$this->SetValueString("IPSTvCl_RecordsHTML", $getRecordings["rec_html"]);
				}
				
			} catch (Exception $e) {	
				$this->SetStatus(202);
			}
        }
                
		// Basisinfos
		public function GetBasicInfo($IPSTvCl_SrvType) {
			try {
			switch ($IPSTvCl_SrvType) {
				case 0:
					$IPSTvCl_IP = $this->ReadPropertyString("IPSTvCl_IP");
					if (Sys_Ping($IPSTvCl_IP, 2000) == false)
					{
						IPS_LogMessage("IPSTvClient", "FEHLER - Der Server antwortet nicht. Daten nicht abrufbar!");
						$this->setStatus(202);
						return;
					}
					
					$IPSTvCl_Username 	= $this->ReadPropertyString("IPSTvCl_Username");
					$IPSTvCl_Password 	= $this->ReadPropertyString("IPSTvCl_Password");
					$IPSTvCl_SrvType 	= $this->ReadPropertyString("IPSTvCl_SrvType");
					$IPSTvCl_Port 		= $this->ReadPropertyString("IPSTvCl_Port");
				
					$IPSTvCl_url = "http://$IPSTvCl_Username:$IPSTvCl_Password@$IPSTvCl_IP:$IPSTvCl_Port/api/serverinfo";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER,false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch, CURLOPT_COOKIESESSION,true);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
					curl_setopt($ch, CURLOPT_USERAGENT, 'IPS');
					curl_setopt($ch, CURLOPT_URL, $IPSTvCl_url);
					$getInfos = curl_exec($ch);
					
					$fehlercode = strpos($getInfos, "401 Unauthorized");
					if ($fehlercode === false) {
						$this->setStatus(102);
						$json_infos = json_decode($getInfos);
						
						$IPSTvCl_serverinfo["sw_version"] = $json_infos->{'sw_version'};
						$IPSTvCl_serverinfo["api_version"] = $json_infos->{'api_version'};
						$IPSTvCl_serverinfo["name"] = $json_infos->{'name'};
						
						return $IPSTvCl_serverinfo;
						
					} else {
						$this->setStatus(201);
						return;
					}
					
			}
			} catch (Exception $e) {	
				$this->SetStatus(203);
				IPS_LogMessage("IPSTvClient", "FEHLER - Meldung: ".$e."!");
			}
		}
		
		// Aufnahmen
		public function GetRecordings($IPSTvCl_SrvType) {
			try {
			switch ($IPSTvCl_SrvType) {
				case 0:
					$IPSTvCl_IP = $this->ReadPropertyString("IPSTvCl_IP");
					if (Sys_Ping($IPSTvCl_IP, 2000) == false)
					{
						IPS_LogMessage("IPSTvClient", "FEHLER - Der Server antwortet nicht. Daten nicht abrufbar!");
						$this->setStatus(202);
						return;
					}
					
					$IPSTvCl_Username 	= $this->ReadPropertyString("IPSTvCl_Username");
					$IPSTvCl_Password 	= $this->ReadPropertyString("IPSTvCl_Password");
					$IPSTvCl_SrvType 	= $this->ReadPropertyString("IPSTvCl_SrvType");
					$IPSTvCl_Port 		= $this->ReadPropertyString("IPSTvCl_Port");
					
					$IPSTvCl_url = "http://$IPSTvCl_Username:$IPSTvCl_Password@$IPSTvCl_IP:$IPSTvCl_Port/api/dvr/entry/grid";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HEADER,false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch, CURLOPT_COOKIESESSION,true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
					curl_setopt($ch, CURLOPT_USERAGENT, 'IPS');
					curl_setopt($ch, CURLOPT_URL, $IPSTvCl_url);
					$getRecordings = curl_exec($ch);
					$fehlercode = strpos($getRecordings, "401 Unauthorized");
					if ($fehlercode === false) {
						$this->setStatus(102);
						$json_recordings = json_decode($getRecordings, true);
					
						$aufnahmenFehlerhaft = 0;
						$aufnahmenErfolgreich = 0;
						$aufnahmenHTML = "<html><body><table>";
						foreach($json_recordings["entries"] as $aufnahmen) {
							if ($aufnahmen["errors"] > 0) {
								$aufnahmenFehlerhaft++;
							} else {
								$aufnahmenErfolgreich++;
							}
							$aufnahmenHTML = $aufnahmenHTML."<tr><td>Sender:&nbsp;".$aufnahmen["channelname"]."&nbsp;Titel:&nbsp;".$aufnahmen["disp_title"]."&nbsp;Dauer:&nbsp;".$this->toStd($aufnahmen["duration"])."</td></tr>";
							$aufnahmenHTML = $aufnahmenHTML."<tr><td colspan=3>".$aufnahmen["disp_description"]."</td></tr>";
						}
						$aufnahmenHTML = $aufnahmenHTML."</table></body></html>";
						
						$IPSTvCl_recordings["rec_total"] = $json_recordings['total'];
						$IPSTvCl_recordings["rec_error"] = $aufnahmenFehlerhaft;
						$IPSTvCl_recordings["rec_success"] = $aufnahmenErfolgreich;
						$IPSTvCl_recordings["rec_html"] = $aufnahmenHTML;
						
						return $IPSTvCl_recordings;
						
					} else {
						$this->setStatus(201);
						return;
					}
			}
			} catch (Exception $e) {	
				$this->SetStatus(203);
				IPS_LogMessage("IPSTvClient", "FEHLER - Meldung: ".$e."!");
			}			
		}
		
        private function SetValueString($Ident, $value)
        {
            $id = $this->GetIDForIdent($Ident);
            if (GetValueString($id) <> $value)
            {
                SetValueString($id, $value);
                return true;
            }
            return false;
        }
        
        private function SetValueInteger($Ident, $value)
        {
            $id = $this->GetIDForIdent($Ident);
            if (GetValueInteger($id) <> $value)
            {
                SetValueInteger($id, $value);
                return true;
            }
            return false;
        }
		
		function toStd($sekunden) 
		{ 
			$stunden = floor($sekunden / 3600); 
			$minuten = floor(($sekunden - ($stunden * 3600)) / 60); 
			$sekunden = round($sekunden - ($stunden * 3600) - ($minuten * 60), 0); 

			if ($stunden <= 9) { 
				$strStunden = "0" . $stunden; 
			} else { 
				$strStunden = $stunden; 
			} 

			if ($minuten <= 9) { 
				$strMinuten = "0" . $minuten; 
			} else { 
				$strMinuten = $minuten; 
			} 

			if ($sekunden <= 9) { 
				$strSekunden = "0" . $sekunden; 
			} else { 
				$strSekunden = $sekunden; 
			} 

			return "$strStunden:$strMinuten:$strSekunden"; 
		} 
    }
?>